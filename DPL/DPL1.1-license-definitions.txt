7. Definitions

7.1 “Affiliate” means a corporation, partnership, or other entity in
which the Licensor or Licensee possesses more than fifty percent (50%) of
the ownership interest, representing the right to make the decisions for
such corporation, partnership or other entity which is now or hereafter,
owned or controlled, directly or indirectly, by Licensor or Licensee.

7.2 “Defensive Patent Claim” means an Infringement Claim against a
DPL User made in response to a pending prior Infringement Claim by said
DPL User against the asserter of the Defensive Patent Claim.

7.3 “Discontinuation Announcement” means a DPL User’s announcement
that:

(a) declares the DPL User’s intent to discontinue offering to license
its Licensed Patents under the DPL, effective as of the Discontinuation
Date; and

(b) contains the DPL User’s contact information for licensing purposes;
and

(c) at least 180 days prior to the Discontinuation Date is posted to a
publicly accessible website; and

(d) at least 180 days prior to the Discontinuation Date is communicated
reasonably and promptly, along with the URL of the website mentioned in
subsection (c) of this provision, by the discontinuing DPL User to every
Licensor of a Patent to which the discontinuing DPL User is a Licensee.

7.4 “Discontinuation Date” means the date a DPL User specifies in
its Discontinuation Announcement to discontinue offering to license its
Licensed Patents under the DPL, which must be at least 180 days after
the date of an individual or entity’s most recent Discontinuation
Announcement.

7.5 “DPL” and “License” mean the grant, conditions, and
limitations herein.

7.6 “DPL User” means an entity or individual that:

(a) has committed to offer a license to each of its Patents under the
DPL; and

(b) has declared such commitment by means of an Offering Announcement; and

(c) if the entity or individual has made a Discontinuation Announcement,
the Discontinuation Date has not yet occurred; and

(d) has not engaged in the conduct described in either Sections 2(e)(i)
or 2(e)(ii).

7.7 “Effective Filing Date” is the effective filing date determined
by the applicable patent office that issued the relevant Licensed Patent.

7.8 “Infringement Claim” means any legal action, proceeding or
procedure for the resolution of a controversy in any jurisdiction in
the world, whether created by a claim, counterclaim, or cross-claim,
alleging patent infringement. Such actions, proceedings, or procedures
shall include, but not be limited to, lawsuits brought in state or
federal court, binding arbitrations, and administrative actions such as
a proceeding before the International Trade Commission.

 7.9 “Licensed Patents” means any and all Patents (a) owned or
 controlled by Licensor; or (b) under which Licensor has the right
 to grant licenses without the consent of or payment to a third party
 (other than an employee inventor).

7.10 “Licensed Products and Services” means any products, services
or other activities of a Licensee that practice one or more claims of
one or more Licensed Patents of a Licensor.

7.11 “Licensee” means any individual, corporation, partnership or
other entity exercising rights granted by the Licensor under this License
including all Affiliates of such entity.

7.12 “Licensor” means any individual, corporation, partnership or
other entity with the right to grant licenses in Licensed Patents under
this License, including any Affiliates of such entity.

 7.13 “Offering Announcement” means a Licensor’s announcement that:

(a) declares the Licensor’s commitment to offer a Defensive Patent
License for any of its Patents to any DPL User; and

(b) contains the Licensor’s contact information for licensing purposes;
and

(c) is posted to a publicly accessible website.

An Offering Announcement may, but is not required to, specify the
particular version of the DPL that the Licensor is committed to
offering. It may also specify a particular version of the DPL “or any
later version” to allow Licensees to accept subsequent new or revised
versions of the DPL.

7.14 “Patent” means any right, whether now or later acquired,
under any national or international patent law issued by a governmental
body authorized to issue such rights. For clarity, this definition
includes any rights that may arise in patent applications, utility
models, granted patents, including, but not limited to, continuations,
continuations-in-part, divisionals, provisionals, results of any patent
reexaminations, and reissues, but excluding design patents or design
registrations.
